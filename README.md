# Botify challenge - Comment App - Désiré N'dri#

Bonjour, comme convenu je reviens vers vous avec l'application de commentaires.

J'ai veillé à respecter les packages et les consignes du document.

J'ai quelque peu lutté sur l'utilisation de redux, mais ce qui était demandé fonctionne et j'ai ajouté la fonctionnalité suppression des commentaires qui fonctionne presque, il reste un correctif à développer lorsqu'il y a plusieurs commentaires en mémoire.

En tout cas j'espère que ça vous plaira, et j'espère à bientot !