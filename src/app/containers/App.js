import React from "react";
import { connect } from "react-redux";

import { Form } from "../components/Form";
import { List } from "../components/List";

class App extends React.Component{

    render(){
        return(
            <div className="container">
                <div className="row">
                    <div className=".col-md-10.col-md-offset-1">
                        <h3>Postez votre commentaire ici</h3>
                        <Form
                            showComments = {this.props.showComments}
                            getCommentData = {this.props.getData.bind(this)}
                        />
                    </div>
                    <div className=".col-md-10.col-md-offset-1">
                        <h3>Liste des commentaires postés</h3>
                        <List
                            comments = {this.props.comments}
                            getTestData= {this.props.getData.bind(this)}
                            deleteComments = {this.props.deleteComment}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        comments: state.seeComments,
        deleted: state.delComment
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        showComments: (comments) => {
            dispatch({
                type: "ADD",
                payload: comments
            });
        },

        deleteComment: (deleted) => {
            dispatch({
                type: "DELETE",
                payload: deleted
            });
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);