import React from "react";

export class List extends React.Component{

    render(){
        let getData = this.props.getTestData;
        let changeState = this.props.deleteComments;


        //Ne fonctionne qu'avec un commentaire en localStorage
        function deleteComment(commentKey) {
            if (localStorage.length === 1){
                delete localStorage[commentKey];
                changeState(getData(localStorage.length));
            }
            else {
                if(localStorage[commentKey + 1]){
                    // TODO
                }
            }
        }

        if (localStorage.length === 0){
            return(
                <div>
                    <p id="commentList">Pas de commentaires pour le moment</p>
                </div>
            )
        }
        else {
            return(
                <div>
                    {this.props.comments.reverse().map((comment, i) =>
                        <div key={i}>
                            <p>L'utilisateur {comment[0]} à laissé un message le {comment[3]}</p>
                            <label>Contenu du commentaire</label>
                            <p>{comment[2]}</p>
                            <button className="waves-effect waves-light btn red" onClick={() => deleteComment(i)}>Effacer le commentaire</button>
                        </div>
                    )}
                </div>
            )
        }
    }
}

// Donne un warning dans la console, bug de la nouvelle version de react
List.propTypes = {
    comments: React.PropTypes.array
};