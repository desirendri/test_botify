import React from "react";

export const Form = (props) => {

        let getData = props.getCommentData;
        let changeState = props.showComments;

        function validateForm(e) {
            // localStorage.clear();
            e.preventDefault();
            let username = document.forms["comment"]["username"].value;
            let email = document.forms["comment"]["email"].value;
            let commentContent = document.forms["comment"]["commentContent"].value;
            let date = new Date;
            let commentArray = [username, email, commentContent, date.toLocaleString()];
            let info = document.getElementById("messages");

            function validateEmail(test) {
                let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(test);
            }

            if (!username) {
                info.innerHTML = "Le nom d'utilsateur ne peut être vide";
            }
            else if (!commentContent) {
                info.innerHTML= "Le contenu du commentaire ne peut être vide";
            }
            else if (email !== "" && validateEmail(email) === false) {
                info.innerHTML = "Mail non valide";
            }
            else {
                localStorage.setItem(localStorage.length.toString(), JSON.stringify(commentArray));
                changeState(getData(localStorage.length));
                let form = document.getElementById("comment");
                form.reset();
                info.innerHTML ="";
            }
        }

        return(
            <form id="comment" name="comment" onSubmit={validateForm}>
                <div>
                    <label>Username (champ requis):</label>
                    <input type="text" name="username" placeholder="Entrez votre nom d'utilisateur"/>
                </div>
                <div>
                    <label>Mail:</label>
                    <input type="text" name="email" placeholder="Entrez votre mail"/>
                </div>
                <div>
                    <label>Commentaire (champ requis):</label>
                    <textarea name="commentContent" placeholder="Ecrivez votre commentaire ici"></textarea>
                </div>
                <div id="messages"></div>
                <div>
                    <input className="waves-effect waves-light btn blue" type="reset" value={"Effacer les champs"}/>
                </div>
                <button className="waves-effect waves-light btn green" type="submit">Poster</button>
            </form>
        );
};