import React from "react";
import { render } from "react-dom";
import { createStore, combineReducers } from  "redux";
import { Provider } from "react-redux";
import App from "./containers/App";

let commentNumber = localStorage.length;
// console.log(localStorage);
// localStorage.clear();

const getCommentData = (commentNumber) =>{
    let commentData = [0];
    if(commentNumber > 0){
        for(let l = 0; l < localStorage.length; l++){
            commentData[l] = JSON.parse(localStorage[l]);
        }
    }
    return commentData;
};

const seeComments = (state = getCommentData(commentNumber), action) => {
    switch(action.type){
        case "ADD":
            state = action.payload;
            break;
    }
    return state;
};

const delComment = (state = getCommentData(commentNumber), action) => {
    switch(action.type){
        case "DELETE":
            state =  action.payload;
            break;
    }
    return state;
};

const appStore = createStore(combineReducers({seeComments, delComment}));

// appStore.subscribe(() => {
//     console.log('changement de state')
// });

render(
    <Provider store={appStore}>
        <App getData = {getCommentData.bind(this)}/>
    </Provider>,
    window.document.getElementById('app'),
);
